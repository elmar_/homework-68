import {
    DELETE_POST,
    FETCH_COUNTER_ERROR,
    FETCH_COUNTER_REQUEST,
    FETCH_COUNTER_SUCCESS, FETCH_TASK_ERROR, FETCH_TASK_REQUEST, FETCH_TASK_SUCCESS, NEW_TASK, POSTING_TASK,
} from "./actions";

const initState = {
    counter: 0,
    error: false,
    loading: true,
    tasks: [],
    newTask: '',
    loadingTask: true,
    errorTask: false
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case FETCH_COUNTER_REQUEST:
            return {...state, loading: true};
        case FETCH_COUNTER_SUCCESS:
            return {...state, counter: action.counter, loading: false}
        case FETCH_COUNTER_ERROR:
            return {...state, error: true, loading: false};
        case FETCH_TASK_REQUEST:
            return {...state, loadingTask: true};
        case FETCH_TASK_SUCCESS:
            return {...state, tasks: action.tasks, loadingTask: false};
        case FETCH_TASK_ERROR:
            return {...state, errorTask: true, loadingTask: false};
        case NEW_TASK:
            return {...state, newTask: action.newTask};
        case POSTING_TASK:
            return {...state, newTask: ''};
        case DELETE_POST:
            return {...state, tasks: [...action.tasks]};
        default:
            return state;
    }
};

export default reducer;
