import axiosFire from "../axiosFire";


export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_ERROR = 'FETCH_COUNTER_ERROR';


export const fetchCounterRequest = () => ({type: FETCH_COUNTER_REQUEST});
export const fetchCounterSuccess = counter => ({type: FETCH_COUNTER_SUCCESS, counter});
export const fetchCounterError = () => ({type: FETCH_COUNTER_ERROR});

export const fetchCounter = () => {
    return async dispatch => {
        dispatch(fetchCounterRequest());

        try {
            const response = await axiosFire.get('counter.json');
            dispatch(fetchCounterSuccess(response.data));
        } catch (e) {
            dispatch(fetchCounterError());
        }
    };
};

export const postCounter = counter => {
    return async dispatch => {
        dispatch(fetchCounterRequest());

        try {
            await axiosFire.put('counter.json', counter);
            dispatch(fetchCounterSuccess(counter));
        } catch (e) {
            dispatch(fetchCounterError());
        }
    };
};

export const NEW_TASK = 'NEW_TASK';
export const newTask = e => ({type: NEW_TASK, newTask: e.target.value});

export const FETCH_TASK_REQUEST = 'FETCH_TASK_REQUEST';
export const FETCH_TASK_SUCCESS = 'FETCH_TASK_SUCCESS';
export const FETCH_TASK_ERROR = 'FETCH_TASK_ERROR';
export const POSTING_TASK = 'POSTING_TASK';

export const fetchTaskRequest = () => ({type: FETCH_TASK_REQUEST});
export const fetchTaskSuccess = tasks => ({type: FETCH_TASK_SUCCESS, tasks});
export const fetchTaskError = () => ({type: FETCH_TASK_ERROR});
export const postingTask = () => ({type: POSTING_TASK});

export const DELETE_POST = 'DELETE_POST';
export const removePost = tasks => ({type: DELETE_POST, tasks})

export const fetchTask = () => {
    return async dispatch => {
        dispatch(fetchTaskRequest());

        try {
            const response = await axiosFire.get('todo.json');
            const tasks = Object.keys(response.data).map(id => ({...response.data[id], id}));
            dispatch(fetchTaskSuccess(tasks));
        } catch (e) {
            throw new Error('error with todo list');
        }
    };
};

export const postTask = text => {
    return async dispatch => {
        dispatch(fetchTaskRequest);

        try {
            await axiosFire.post('todo.json', {text});
            dispatch(postingTask());
        } catch (e) {
            dispatch(fetchTaskError());
        }
    };
};




