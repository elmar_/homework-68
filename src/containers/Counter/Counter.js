import React, {useEffect} from 'react';
import './Counter.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchCounter, postCounter} from "../../store/actions";
import Spinner from "../../components/Spinner/Spinner";

const Counter = () => {
    const dispatch = useDispatch();
    const state = useSelector(state => state);

    useEffect(() => {
        dispatch(fetchCounter())
    }, [dispatch]);

    const increaseCounter = () => dispatch(postCounter(state.counter + 1));
    const decreaseCounter = () => dispatch(postCounter(state.counter - 1));
    const plusCounter = () => dispatch(postCounter(state.counter + 5));
    const minusCounter = () => dispatch(postCounter(state.counter - 5));
    
    if (state.error) {
        throw new Error('error with counter');
    }

    let program = (
        <div className="Counter">
            <h1>{state.counter}</h1>
            <button onClick={increaseCounter}>Increase</button>
            <button onClick={decreaseCounter}>Decrease</button>
            <button onClick={plusCounter}>Increase by 5</button>
            <button onClick={minusCounter}>Decrease by 5</button>
        </div>
    );

    if (state.loading) {
        program = <Spinner />
    }

    return (
        <>
            {program}
        </>
    );
};

export default Counter;