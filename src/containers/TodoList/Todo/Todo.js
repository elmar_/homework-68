import React, {useEffect} from 'react';
import './Todo.css';
import Task from "../../../components/Task/Task";
import AddTaskForm from "../AddTaskForm/AddTaskForm";
import {useDispatch, useSelector} from "react-redux";
import {fetchTask, postTask, removePost} from "../../../store/actions";
import axiosFire from "../../../axiosFire";

const Todo = () => {

    const taskState = useSelector(state => state);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchTask());
    }, [dispatch]);

    const addTask = async () => {
        await dispatch(postTask(taskState.newTask));
        window.location.reload();
    };

    const removeTask = async id => {
        const copyTasks = taskState.tasks;
        copyTasks.splice(copyTasks.findIndex(obj => obj.id === id), 1);
        dispatch(removePost(copyTasks));
        await axiosFire.delete('todo/' + id + '.json');
    };

    let tasksList = (
        <>
            {
                taskState.tasks.map(task => {
                    return <Task text={task.text} key={task.id} remove={() => removeTask(task.id)} />
                })
            }
        </>
    );

    return (
        <div className="Todo">
            <AddTaskForm
                onBtnClick={addTask}
            />
            {tasksList}
        </div>
    );
};

export default Todo;