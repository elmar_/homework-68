import React from 'react';
import './AddTaskForm.css';
import {newTask} from "../../../store/actions";
import {useDispatch, useSelector} from "react-redux";

const AddTaskForm = props => {
    const task = useSelector(state => state.newTask);
    const dispatch = useDispatch();
    const createTask = e => dispatch(newTask(e));

    return(
        <div className="AddTaskForm">
            <input type="text" className="input" onChange={createTask} value={task}/>
            <button type="button" className="btn" onClick={props.onBtnClick}>Add</button>
        </div>
    )
}
export default AddTaskForm;