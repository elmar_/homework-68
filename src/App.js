import React from 'react';
import Counter from "./containers/Counter/Counter";
import Todo from "./containers/TodoList/Todo/Todo";
import './App.css';

const App = () => (
    <div className="App">
        <Counter />
        <Todo />
    </div>
);

export default App;