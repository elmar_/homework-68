import axios from "axios";

const axiosFire = axios.create({
    baseURL: 'https://classwork-63-burger-default-rtdb.firebaseio.com/'
});

export default axiosFire;
